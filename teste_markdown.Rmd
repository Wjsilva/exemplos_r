---
title: "exemplo rmarkdown"
author: "Wesley J. Silva"
date: "24 de agosto de 2016"
output: 
  html_document: 
    keep_md: yes
---

```{r,include=F, eval = T}
executar_download = F
```

# SOBRE

Este texto foi produzido durante o I Workshop de R realizado no Instituto de Pesquisa Econômica Aplicada (Ipea). O *script* utilizado para produzir esta documentação segue [no gitlab](https://gitlab.com/Wjsilva/exemplos_r/tree/master), acessível ao público. Nos links também seguem exemplos reproduzidos localmente sobre construção de aplicativo shiny.


# Esta é uma seção

Aqui é o meu texto, e eu pretendo criar uma subeção dentro desta seção

## Esta é uma subseção

aqui é o corpo do texto da minha subseção. Vamos criar, agora, uma sub-subseção

### Esta é uma sub-subseção


Texto.


# Ilustrando listagem em tópicos

No exemplo a seguir, vamos criar uma listagem:

* elemento 1
* outro elemento
* mais um
    - um subtópico

# Negrito e itálico

Estou escrevendo um texto, e **quero destacar esse trecho, em negrito**.

Agora, eu *quero destacar este trecho em itálico.*


# Inserindo fórmulas


Posteriormente, vamos exemplicar a contrução automatizada de gráficos
e estatísticas com base em valores simulados de uma distribuição normal,
considerando média 0 e desvio-padrão 1.

A densidade de probabilidade da distribuição Normal(0,1) é:

$$f(x) = e^{\frac{x^2}{2}}$$


# Simulando dados da distribuição Normal


No código abaixo, simulamos uma distribuição normal

```{r}
dados_normal <- rnorm(150)
```


# Criando gráfico da distribuição simulada

Abaixo segue o histograma dos dados simulados acima


```{r}
hist(dados_normal)
```

# Recriando outputs, sem mostrar código

```{r,echo = F}
dados_gamma <- rgamma(150,1)

hist(dados_gamma)
```

# Mostrar código, sem executá-lo

Vamos mostrar como se faz uma regressão linear no R. Para isso, vamos
considerar como variável resposta a incidência de pobreza (*t_pbz*),
em função da proporção de beneficiários do bolsa-família (*t_pbf*) 
e do pronaf (*t_pronaf*).


```{r, include = T, eval = F}
lm(t_pbz ~ t_pbf + t_pronaf)
```


# Exemplo de trecho condicionado

Nas etapas anteriores, simulamos dados de uma distribuição normal.

A média desses valores simulados é `r round(mean(dados_normal),2)`. Já o
desvio-padrão é `r round(sd(dados_normal),2)`.


## outra forma de condicionamento

```{r,echo = F, results = 'asis'}
linha_mtcars <- mtcars[15,]
comando_md <- paste0("* **",names(linha_mtcars),"**: ",linha_mtcars)
comando_md <- paste(comando_md, collapse = "\n") 
cat(comando_md)

```

# Tabela, link e imagem

## Tabela

Vamos criar uma tabela com os dados das 5 primeiras linhas da base 'mtcars'

```{r,echo = F}
linhas5 <- head(mtcars,5)
knitr::kable(linhas5)
```


## Link para pagina e imagem

Inserindo um link para a [galeria shiny](http://shiny.rstudio.com/gallery/).

Inserindo uma imagem.

![legenda aqui](http://www.estudokids.com.br/wp-content/uploads/2015/04/por-que-a-terra-e-redonda.jpg)


































































