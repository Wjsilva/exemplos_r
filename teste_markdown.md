# exemplo rmarkdown
Wesley J. Silva  
24 de agosto de 2016  



# SOBRE

Este texto foi produzido durante o I Workshop de R realizado no Instituto de Pesquisa Econômica Aplicada (Ipea). O *script* utilizado para produzir esta documentação segue [no gitlab](https://gitlab.com/Wjsilva/exemplos_r/tree/master), acessível ao público. Nos links também seguem exemplos reproduzidos localmente sobre construção de aplicativo shiny.


# Esta é uma seção

Aqui é o meu texto, e eu pretendo criar uma subeção dentro desta seção

## Esta é uma subseção

aqui é o corpo do texto da minha subseção. Vamos criar, agora, uma sub-subseção

### Esta é uma sub-subseção


Texto.


# Ilustrando listagem em tópicos

No exemplo a seguir, vamos criar uma listagem:

* elemento 1
* outro elemento
* mais um
    - um subtópico

# Negrito e itálico

Estou escrevendo um texto, e **quero destacar esse trecho, em negrito**.

Agora, eu *quero destacar este trecho em itálico.*


# Inserindo fórmulas


Posteriormente, vamos exemplicar a contrução automatizada de gráficos
e estatísticas com base em valores simulados de uma distribuição normal,
considerando média 0 e desvio-padrão 1.

A densidade de probabilidade da distribuição Normal(0,1) é:

$$f(x) = e^{\frac{x^2}{2}}$$


# Simulando dados da distribuição Normal


No código abaixo, simulamos uma distribuição normal


```r
dados_normal <- rnorm(150)
```


# Criando gráfico da distribuição simulada

Abaixo segue o histograma dos dados simulados acima



```r
hist(dados_normal)
```

![](teste_markdown_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

# Recriando outputs, sem mostrar código

![](teste_markdown_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

# Mostrar código, sem executá-lo

Vamos mostrar como se faz uma regressão linear no R. Para isso, vamos
considerar como variável resposta a incidência de pobreza (*t_pbz*),
em função da proporção de beneficiários do bolsa-família (*t_pbf*) 
e do pronaf (*t_pronaf*).



```r
lm(t_pbz ~ t_pbf + t_pronaf)
```


# Exemplo de trecho condicionado

Nas etapas anteriores, simulamos dados de uma distribuição normal.

A média desses valores simulados é -0.19. Já o
desvio-padrão é 0.92.


## outra forma de condicionamento

* **mpg**: 10.4
* **cyl**: 8
* **disp**: 472
* **hp**: 205
* **drat**: 2.93
* **wt**: 5.25
* **qsec**: 17.98
* **vs**: 0
* **am**: 0
* **gear**: 3
* **carb**: 4

# Tabela, link e imagem

## Tabela

Vamos criar uma tabela com os dados das 5 primeiras linhas da base 'mtcars'


                      mpg   cyl   disp    hp   drat      wt    qsec   vs   am   gear   carb
------------------  -----  ----  -----  ----  -----  ------  ------  ---  ---  -----  -----
Mazda RX4            21.0     6    160   110   3.90   2.620   16.46    0    1      4      4
Mazda RX4 Wag        21.0     6    160   110   3.90   2.875   17.02    0    1      4      4
Datsun 710           22.8     4    108    93   3.85   2.320   18.61    1    1      4      1
Hornet 4 Drive       21.4     6    258   110   3.08   3.215   19.44    1    0      3      1
Hornet Sportabout    18.7     8    360   175   3.15   3.440   17.02    0    0      3      2


## Link para pagina e imagem

Inserindo um link para a [galeria shiny](http://shiny.rstudio.com/gallery/).

Inserindo uma imagem.

![legenda aqui](http://www.estudokids.com.br/wp-content/uploads/2015/04/por-que-a-terra-e-redonda.jpg)


































































